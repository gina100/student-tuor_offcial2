﻿using AngularLogin4.Models;
using System.Collections.Generic;

namespace AngularLogin4.Interfaces
{
    public interface IBooking
    {
        IEnumerable<Booking> GetAllBookings();
        int AddBooking(Booking booking);
        int UpdateBooking(Booking booking);
        Booking GetBookingData(int id);
        int DeleteBooking(int id);
    }
}
