import { Component } from '@angular/core';
declare const mapLoader: any;
declare const GetRoute: any;
declare const calculateAndDisplayRoute: any;
declare const initMap: any;
import * as $ from 'jquery';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent  {
 
  public myMap()
  {
    var source = document.getElementById('travelfrom');
    var destination = document.getElementById('travelto');
    var mapdv = document.getElementById('dvMap');
    initMap(mapdv, source, destination);
    //GetRoute(source, destination);
  }
  public GetRoute()
  {
    var mapdv = document.getElementById('dvMap');
    var source = document.getElementById("travelfrom");
    console.log(source);
    var destination = document.getElementById("travelto");

    console.log(destination);
    var destinations = document.getElementById("destinations");
    var table = document.getElementById("tblResults");
    var totdistance = document.getElementById("totdistance");

    GetRoute(source, destination);
  }



}
