import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import *as $ from 'jquery';

@Component({
  selector: 'app-add-booking',
  templateUrl: './add-booking.component.html'
})



export class AddBookingComponent implements OnInit  {
  ngOnInit() {
    this.myFunction();
  }

  public myFunction()
  {
    $.ajax({
      type: "Post",
      url: "/StudentDetails/GetAllStudentDetails",
      success: function (data) {
        var select = document.getElementById('#seniorStudentId');
        $('#seniorStudentId').empty();
        $('#seniorStudentId').append($('<option></option>').val('').html('Select Student'));
        //For loop to get all events in database
        $.each(data, function (i, v) {

          $('#seniorStudentId').append($('<option></option>').val(v.staffMemberId).html(v.title));

        })
      },
      //Catch error of loading
      error: function (error) {
        alert('Failed to load all students!')
      },
    });
    $.ajax({
      type: "Post",
      url: "/StudentDetails/GetAllStudentDetails",
      success: function (data) {
        var select = document.getElementById('#studentId');
        $('#studentId').empty();
        $('#studentId').append($('<option></option>').val('').html('Select Student'));
        //For loop to get all events in database
        $.each(data, function (i, v) {

          $('#studentId').append($('<option></option>').val(v.staffMemberId).html(v.title));

        })
      },
      //Catch error of loading
      error: function (error) {
        alert('Failed to load all students!')
      },
    });

  }

  //Save Event Button
  public SaveData()
  {
    if ($('#scheduleStartDate').val() == "") {
      alert('Start Date required')
      return;
    }
    if ($('#scheduleEndDate').val() == "") {
      alert('End Date required')
      return;
    }
    else {
      var startDate = moment($('#scheduleStartDate').val(), "MM/DD/YYYY HH:mm A").toDate();
      var endDate = moment($('#scheduleEndDate').val(), "MM/DD/YYYY HH:mm A").toDate();
      if (startDate > endDate) {
        alert('End date cannot be before Start date');
        return;
      }
      if (startDate < new Date()) {
        alert('Start date has Already Passed');
        return;
      }


      var vr = new Date(startDate);
      var ve = new Date(endDate);
      var m = 2;
      var minTime = new Date(2016, m, m, 3);
      var maxTime = new Date(2016, m, m, 18);
      if ((vr.getHours() < minTime.getHours()) || (vr.getHours() > maxTime.getHours()) || (ve.getHours() < minTime.getHours()) || (ve.getHours() > maxTime.getHours())) {
        alert('Select appropriate Bussiness Time!!!');
        return;
      }
      var startDate1 = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDay(), startDate.getHours() + 2, startDate.getMinutes());
      var endDate1 = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDay(), endDate.getHours() + 2, endDate.getMinutes());
    }

    var Booking = {

      BookingId: $('#bookingId').val(),
      Title: $('#title').val(),
      Description: $('#description').val(),
      ScheduleStartDate: $('#scheduleStartDate').val(),
      ScheduleEndDate: $('#scheduleEndDate').val(),
      ThemeColor: $('#themeColor').val(),
      Location: $('#location').val(),
      SeniorStudentId: parseInt($('#seniorStudentId').val()),
      StudentId: parseInt($('#studentId').val()),
      Archived: false

    }
    console.log(Booking);
    //SaveEvent(data);
    $.ajax({//Ajax is used for to call controller to get and post data.
      method: 'Get',
      url: '/Booking/Create2',
      data: Booking,
      dataType: "json",
      contentType: 'application/json',
      success: function (data)
      {
        var newUrl: 'https://localhost:5001/fetch-calendar';
        window.location.href = newUrl;
        //alert("<a class='nav - link text - dark'[routerLink] = '["+'/fetch-calendar'+"]' > Calendar < /a>");
      }, error: function (error) {
        alert('Plaese enter all inputs!')
      }
    })

  }

}
