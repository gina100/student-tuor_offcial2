import { Component, OnInit} from '@angular/core';
declare const myTest: any;
declare const loader: any;
declare const initMap: any;
declare const GetRoute: any;
import $ from 'jquery';
@Component({
  selector: 'app-fetch-calendar',
  templateUrl: './fetch-calendar.component.html'
  
})
export class FetchCalendarComponent implements OnInit {
  ngOnInit() {
    loader();
  }
  public myFunction()
  {
    loader();
  }
  public myMap() {
    var source = document.getElementById('travelfrom');
    var destination = document.getElementById('travelto');
    var mapdv = document.getElementById('dvMap');
    initMap(mapdv, source, destination);
  }
  GetRoute()
  {
    var source = document.getElementById('travelfrom');
    var destination = document.getElementById('travelto');
    var mapdv = document.getElementById('dvMap');
    GetRoute(mapdv, source, destination);
  }
}

