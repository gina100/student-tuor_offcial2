﻿using System;
using System.Collections.Generic;

namespace AngularLogin4.Models
{
    public partial class StudentDetails
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public string StudentLevel { get; set; }
    }
}
