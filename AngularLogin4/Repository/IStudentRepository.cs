﻿using AngularLogin4.Models;
using AngularLogin4.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularLogin4.Repository
{
    public interface IStudentRepository
    {
        IQueryable<Student> Get();
        IQueryable<Student> Get(string ? id);

        Student Add(Student student);
        void Delete(string id);
        void Update(Student student);
        Student Find(string ? id);
    }
}
