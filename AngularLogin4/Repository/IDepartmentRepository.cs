﻿using AngularLogin4.Models;
using AngularLogin4.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularLogin4.Repository
{
    public interface IDepartmentRepository
    {
        IQueryable<Department> Get();
        IQueryable<Department> Get(int? id);

        Department Add(Department department);
        void Delete(int id);
        void Update(Department department);
        Department Find(int? id);
    }
}
