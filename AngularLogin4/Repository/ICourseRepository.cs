﻿using AngularLogin4.Data;
using AngularLogin4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularLogin4.Repository
{
   public interface ICourseRepository
    {
        IQueryable<Course> Get();
        IQueryable<Course> Get(int? id);

        Course Add(Course course);
        void Delete(int id);
        void Update(Course course);
        Course Find(int? id);
    }
}
