﻿using AngularLogin4.Interfaces;
using AngularLogin4.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularLogin4.DataAccess
{
    public class ScheduleDataAccessLayer : ISchedule
    {
        private myTestDBContext db;

        public ScheduleDataAccessLayer(myTestDBContext _db)
        {
            db = _db;
        }

        public IEnumerable<Schedule> GetAllSchedules()
        {
            try
            {
                return db.Schedule.ToList().OrderBy(x => x.ScheduleId);
            }
            catch
            {
                throw;
            }
        }

        //To Add new employee record 
        public int AddSchedule(Schedule Schedule)
        {
            try
            {
                db.Schedule.Add(Schedule);
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

        //To Update the records of a particluar employee
        public int UpdateSchedule(Schedule Schedule)
        {
            try
            {
                db.Entry(Schedule).State = EntityState.Modified;
                db.SaveChanges();

                return 1;
            }
            catch
            {
                throw;
            }
        }

        //Get the details of a particular employee
        public Schedule GetScheduleData(int id)
        {
            try
            {
                Schedule Schedule = db.Schedule.Find(id);
                return Schedule;
            }
            catch
            {
                throw;
            }
        }

        //To Delete the record on a particular employee
        public int DeleteSchedule(int id)
        {
            try
            {
                Schedule Schedule = db.Schedule.Find(id);
                db.Schedule.Remove(Schedule);
                db.SaveChanges();
                return 1;
            }
            catch
            {
                throw;
            }
        }

    }
}
